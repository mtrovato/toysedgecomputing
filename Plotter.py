import sys
import matplotlib.pyplot as plt
import argparse
import numpy as np
import math
from distutils.util import strtobool

def removePartNoHits(fileName, verbose=False):
    f = open(fileName, 'r')
    newPartCnt = 0
    iline = 0
    line_tobeerased = []
    for line in f.readlines():
        line_split = line.split(",")
        if (line_split[0].find("Part (mu^+)") != -1):
            if newPartCnt == 3: #previous particle had no hits
                line_tobeerased.append(iline-2)
                line_tobeerased.append(iline-1)
                line_tobeerased.append(iline)                
            newPartCnt = 1
        elif newPartCnt == 1:
            newPartCnt = 2
        elif newPartCnt == 2:
            newPartCnt = 3
        elif newPartCnt == 3:
            newPartCnt = 0

        iline = iline + 1

    if newPartCnt == 3: #file ended with particle with no hits
        line_tobeerased.append(iline-3)
        line_tobeerased.append(iline-2)
        line_tobeerased.append(iline-1)                
        

    #print ("removing lines",line_tobeerased)
    f.close()
    f = open(fileName, 'r')
    fileNewName = fileName.split(".")[0]+"_new.txt"
    fout = open(fileNewName,"w")
    iline = 0
    for line in f.readlines():
        toberemoved = False
        for i in range(0, len(line_tobeerased)):
            if line_tobeerased[i] == iline:
                if verbose:
                    print ("remove",line,"since particle outside acceptance")
                toberemoved = True            
        if not toberemoved:
            fout.write(line)

        iline = iline + 1

    f.close()
    fout.close()
    return fileNewName

def readHits(fileName, iPartRange, pTSel=[0,1000], verbose=False):
    if verbose:
        print ("---readHits---")
    f = open(fileName, 'r')
    hits = [] #[ [[row, col, layerN, segN]_0,...[row, col, layerN, segN]_nhits_0]] , ..., [[row, col, layerN, segN]_0, ..., [row, col, layerN, segN]_nhits_M]]] #M = #particles with hits
    partN = -1
    hits_part = []
    partInfo = [] #[ [X,Y,Z,pX,pY,pZ]_0 , ..., [X,Y,Z,pX,pY,pZ]_O] # O = number of particles
    storeInfo = False
    toDiscard = False
    ip = 0
    for line in f.readlines():
        line_split = line.split(",")
        if verbose:
            print (line_split)
        if (line_split[0].find("Part") != -1):
            if (line_split[0].find("Part (mu^+)") != -1):
                storeInfo = True
            continue
        if storeInfo:
            X  = line_split[0] 
            Y  = line_split[1] 
            Z  = line_split[2] 
            pX = line_split[3] 
            pY = line_split[4] 
            pZ = line_split[5] 
            pT = np.sqrt(np.add(np.multiply(float(pX),float(pX)),np.multiply(float(pY),float(pY))))
            if verbose:
                print ("ip, iPartRange",ip,iPartRange)
            if (
                    (pT > pTSel[1] or pT < pTSel[0]) or
                    (ip < iPartRange[0] or ip > iPartRange[1])
            ):
                toDiscard = True
            else:
                toDiscard = False
                partInfo.append([X,Y,Z,pX,pY,pZ])
            
            ip = ip + 1
            if verbose:
                print ("toDiscard",toDiscard)

            storeInfo = False            
            continue

        if (ip-1) > iPartRange[1]: # no need to continue
            hits_part_c = hits_part.copy() 
            hits.append(hits_part_c)
            partN = -1
            hits_part.clear()
            return hits, partInfo
 


        if toDiscard:
            continue
            
        row    = line_split[1]
        col    = line_split[2]
        layerN = line_split[3]
        segN   = line_split[4]
         
        if partN == -1: #first time
            partN  = line_split[0]
            hits_part.append([row, col, layerN, segN])
        elif line_split[0] == partN: #same partN as previous iteration
            partN  = line_split[0]
            row    = line_split[1]
            col    = line_split[2]
            layerN = line_split[3]
            segN   = line_split[4]
            hits_part.append([row, col, layerN, segN])
        else: #new part -> save prev part info
            hits_part_c = hits_part.copy() 
            hits.append(hits_part_c)
            partN = line_split[0] 
            hits_part.clear()
            hits_part.append([row, col, layerN, segN])

        # if len(hits) == npart:
        #     #npart+1 saved npart hit set saved, remove last particle since it was only used to save last hit set ("see #else: new part -> save prev part info")
        #     partInfo.pop()
        #     return hits, partInfo

    if len(hits_part) > 0:
        hits_part_c = hits_part.copy() 
        hits.append(hits_part_c)
        partN = -1
        hits_part.clear()

    return hits, partInfo

def nHitsVspT(pT_bins, pTinv_bins, hits, layerN, partInfo, verbose=False):
    nHitsVspT_array = [] #[ [pT_bin_0, npart_0 [[pX_0, pY_0, pZ_0, nhits_0],...,[pX_0N, pY_0N, pZ_0N, nhits_0_npart_0]]], ..., [pT_bin_M, []],.., [[pT_bin_T], [[pX_T0, pY_T0, pZ_T0, nhits_T0],...,[pX_TC, pY_TC, pZ_TC, nhits_T_npart_T]]] ]  
    if verbose:
        print ("---nHitsVspT---")
    npart = len(hits)
    if verbose:
        print ("npart",npart)
    for ib in range(0, len(pT_bins)):
        nHitsVspT_array.append([float(pT_bins[ib]),0,[]]) 
    for ib in range(0, len(pTinv_bins)):
        nHitsVspTinv_array.append([float(pTinv_bins[ib]),0,[]]) 
    
    for ip in range(0,npart):
        nHits = nHitsPerLayer(hits[ip],layerN)
        if nHits == 0:
            continue
        if verbose:
            print ("ip, layer=",ip,layerN,"nhits=",nHits)
        pX = float(partInfo[ip][3])
        pY = float(partInfo[ip][4])
        pZ = float(partInfo[ip][5])
        pT = np.sqrt(np.add(np.multiply(pX,pX),np.multiply(pY,pY)))
        if verbose:
            print ("pX,pY,pZ,pT",pX,pY,pZ,pT)
        for ib in range(0, len(nHitsVspT_array)-1):
            if pT>= nHitsVspT_array[ib][0] and pT< nHitsVspT_array[ib+1][0]:
                if verbose:
                    print ("in bin pt: [",nHitsVspT_array[ib][0],nHitsVspT_array[ib+1][0],"]")
                nHitsVspT_array[ib][1] = nHitsVspT_array[ib][1]+1 #increase npart in that bin
                pT_bin_tmp = nHitsVspT_array[ib][2]
                pT_bin_tmp.append([pX, pY, pZ, nHits])
                nHitsVspT_array[ib][2] = pT_bin_tmp
        for ib in range(0, len(nHitsVspTinv_array)-1):
            if 1/pT>= nHitsVspTinv_array[ib][0] and 1/pT< nHitsVspTinv_array[ib+1][0]:
                if verbose:
                    print ("in bin ptinv: [",nHitsVspTinv_array[ib][0],nHitsVspTinv_array[ib+1][0],"]")
                nHitsVspTinv_array[ib][1] = nHitsVspTinv_array[ib][1]+1 #increase npart in that bin
                pTinv_bin_tmp = nHitsVspTinv_array[ib][2]
                pTinv_bin_tmp.append([pX, pY, pZ, nHits])
                nHitsVspTinv_array[ib][2] = pTinv_bin_tmp

    if verbose:
        print ("nHitsVspT_array=",nHitsVspT_array)
        print ("nHitsVspTinv_array=",nHitsVspTinv_array)

#    npart_check=0
#    for ib in range(0, len(nHitsVspT_array)):
#        npart_check = npart_check+nHitsVspT_array[ib][1]
#    if npart_check != npart:
#        print ("npart from nHitsVspT_array and npart from hits do not match",npart_check,npart)
#        sys.exit()

    return nHitsVspT_array, nHitsVspTinv_array

def nHitsPerLayer(hits,layerN):
    nHits = 0
    if layerN < 0:
        nHits = len(hits)
    else:
        for ih in range(0,len(hits)):
            if int(hits[ih][2]) == layerN:
                nHits = nHits + 1

    return nHits

def HitsPerLayer(hits,layerN):
    hit = [] #row, col, seg
    for ih in range(0,len(hits)):        
        if int(hits[ih][2]) == layerN:
            row = int(hits[ih][0])
            col = int(hits[ih][1])
            seg = int(hits[ih][3])
            hit.append([row, col, seg])
    return hit

def compMeanStdDev(X, verbose = False):
    if verbose:
        print ("***compMeanStdDev***")
    mean_array = []
    stddev_array = []
    for ippT in range(0,len(X)):
        bin         = X[ippT]
        binPTMin    = bin[0]
        npart_ptbin = bin[1]
        binCont     = bin[2]
        if npart_ptbin != len(bin[2]):
            print ("npart_ptbin != len(pTbin[2])",npart_ptbin,len(bin[2]))
            sys.exit()

        if verbose:
            print ("binPTMin=",binPTMin)
            print ("npart_ptbin",npart_ptbin)
            print ("binCont",binCont)

        mean = 0
        stddev = 0
        mean_tmp = 0
        stddev_tmp = 0

        for ip in range(0,npart_ptbin):        
            nHits = binCont[ip][3]
            if nHits == 0:
                print ("nHits should be >0", nHits)
                sys.exit()
            mean_tmp = mean_tmp + nHits
        if npart_ptbin>0:
            mean = mean_tmp/npart_ptbin    
        for ip in range(0,npart_ptbin):        
            nHits = binCont[ip][3]
            stddev_tmp = stddev_tmp + pow((nHits-mean),2)
        if npart_ptbin>0:
            stddev = math.sqrt(stddev_tmp/npart_ptbin)

        mean_array.append(mean)
        stddev_array.append(stddev)

    if verbose:
        print ("mean_array=",mean_array)
        print ("stddev_array=",stddev_array)
        
    return mean_array, stddev_array

#def generateSensorMatrix(hitList, nRow, nCol, nSeg, verbose=False):
def generateSensorMatrix(hitList, nRow, nCol, verbose=False):
    #fill nRowxnCol 0 matrix with 1's according to the hitList location
    #return [rowMin,rowMax]x[colMin,colMax] boundaries
    #output = np.zeros((nRow,nCol,nSeg), dtype=np.int8) #too slow. Unlikely to have the same row, col from different seg since the end of one is adjacent to the beginning of next one
    output = np.zeros((nRow,nCol), dtype=np.int8)
    rowBoundaries = []
    colBoundaries = []
    seg_array = []
    for ih in range(len(hitList)):
        row = hitList[ih][0]
        col = hitList[ih][1]
        seg = hitList[ih][2]
        iseg = -1
        if (
                row >= 0 and row <= nRow and
                col >= 0 and col <= nCol 
        ):
            output[row,col] = 1
            
            #find iterator corresponding to seg
            for i in range(0,len(seg_array)):
                if seg_array[i] == seg:
                    iseg = i
            #if not found append a new one -> iterator is the last
            if iseg == -1:
                seg_array.append(seg)
                iseg = len(seg_array)-1
                rowBoundaries.append([999,-999])
                colBoundaries.append([999,-999])

            if row < rowBoundaries[iseg][0]:
                rowBoundaries[iseg][0] = row
            if row > rowBoundaries[iseg][1]:
                rowBoundaries[iseg][1] = row
            if col < colBoundaries[iseg][0]:
                colBoundaries[iseg][0] = col
            if col > colBoundaries[iseg][1]:
                colBoundaries[iseg][1] = col
            if verbose:
                print ("Input Hits (Row,Col,Seg)=",row,",",col,",",seg)
        else:
            print("WARNING: Hit (Row,Col)=",row,",",col," not in range...skipped")

    np.set_printoptions(threshold=sys.maxsize)
    
    for irc in range(0,len(rowBoundaries)):
        rowBoundaries[irc][0] = max(0,rowBoundaries[irc][0]-5)
        rowBoundaries[irc][1] = rowBoundaries[irc][1]+5
        colBoundaries[irc][0] = max(0,colBoundaries[irc][0]-5)
        colBoundaries[irc][1] = colBoundaries[irc][1]+5

    if verbose:
        #print ("output=",output)
        print ("rowMin,rowMax=",rowBoundaries)
        print ("colMin,colMax=",colBoundaries)
        print ("seg=",seg_array)


    return rowBoundaries, colBoundaries, seg_array, output

def plot2D(xLabels, Y, DY, xLabel, figName, verbose=False):
    if verbose:
        print ("---plot2D---")
        print ("xLabels=",xLabels)
        print ("Y=",Y)
        print ("DY=",DY)
    plt.figure()
    ax = plt.subplot()
    x_pos = np.arange(len(xLabels))
    #compute asymmtrical error so to avoid cases where error bar goes negative
    DYm, DYp = asymError(Y, DY)
    ax.bar(x_pos,Y,yerr=[DYm,DYp],align='center')

    ax.set_xticks(x_pos)
    ax.set_xticklabels(xLabels)
    ax.set_xlabel(xLabel)
    ax.set_ylabel("#Hits")

    ax.yaxis.grid(True)
    plt.savefig(figName)
    #close figure to save memory
    plt.cla()
    plt.close()


def plotImage(matrix, cmaps, Title, strFile, SetTicks, verbose=False):
    image = matrix[3]
    zoomX_array = matrix[0]
    zoomY_array = matrix[1]
    seg_array = matrix[2]
    for iz in range(0,len(zoomX_array)):
        fig,axes=plt.subplots(1,1)
        zoomX = zoomX_array[iz]
        zoomY = zoomY_array[iz]
        seg = seg_array[iz]
        if(zoomX[1]>zoomX[0] and zoomY[1]>zoomY[0]):
            image_zoom = image[zoomX[0]:zoomX[1],zoomY[0]:zoomY[1]]
            if verbose:
                print ("zoomed image=",image_zoom)
            plt.imshow(image_zoom, cmap=cmaps, origin='upper') #looks complementary to gray
            if verbose:
                print ("zoomX, zoomY=",zoomX,zoomY)
            if SetTicks:
                axes.xaxis.set_ticks(np.arange(-0.5,zoomY[1]-zoomY[0],1))
                axes.yaxis.set_ticks(np.arange(-0.5,zoomX[1]-zoomX[0],1))
        else:
            print ("zoomX[1]>zoomX[0] and zoomY[1]>zoomY[0]", zoomX[0], zoomX[1], zoomY[0], zoomY[1])
            sys.exit()

        plt.xlabel("cols")
        plt.ylabel("rows")
        plt.grid(True)

        plt.title(Title+"-seg"+str(seg))

        #matplotlib.pyplot.colorbar
        cax= plt.axes([0.85, 0.1, 0.075, 0.8])
        plt.colorbar(cax=cax)

        if(strFile==''):
            plt.show(block=True) #False: let's the script continue, to be debugged
        else:
            plt.savefig(strFile+"-seg"+str(seg)+'.png')

    #close figure to save memory
    plt.cla()
    plt.close()
    
def asymError(Y, DY):
    DYp = []
    DYm = []
    for idy in range(0,len(DY)):
        mean = Y[idy]
        err  = DY[idy]
        DYp.append(err)
        if ((mean-err)<0):
            DYm.append(mean)
        else:
            DYm.append(err)

    return DYm, DYp

if __name__ == '__main__':
    dir = "Plotter/"
    subdir = "Shapes/" #must exist in dir before running
    verbose = False
    plotShapes = False
    nPart = -1
    iPartRange = [-1,99999]
    nLayer = 5
    layerN = -1
    nRow = 773
    nCol = 805
    pTSel = [0,1000]
    #nSeg = 1000 #check the number

    RowColSegHitLayer = [] #local hit position for a given layer (or all layers if layerN <0): (row, col, seg)
    matrix = [] # rowBoundaries, colBoundaries, segment list, hit matrix (nRowxnCol matrix with all 0's except in the hit position). TO DO: 3D matrix (include seg) is too slow and it's a waste   
    pX_array = [] #pX array
    pY_array = []
    pZ_array = []
    hits = [] #[ [[row, col, layerN, segN]_0,...[row, col, layerN, segN]_nhits_0]] , ..., [[row, col, layerN, segN]_0, ..., [row, col, layerN, segN]_nhits_M]]] #M = #particles with hits
    nHitsVspT_array   = [] #[ [pT_bin_0, npart_0 [[pX_0, pY_0, pZ_0, nhits_0],...,[pX_0N, pY_0N, pZ_0N, nhits_0_npart_0]]], ..., [pT_bin_M, []],.., [[pT_bin_T], [[pX_T0, pY_T0, pZ_T0, nhits_T0],...,[pX_TC, pY_TC, pZ_TC, nhits_T_npart_T]]] ]  
    nHitsVspTinv_array   = []
    nHitsMean_array   = [] #<hits> Vs pT
    nHitsStddev_array = []
    nHitsVspTinvMean_array   = [] #<hits> Vs 1/pT
    nHitsVspTinvStddev_array = []
    nPartVspT_array       = [] #nPart Vs pT
    nPartVspTErr_array    = []
    nPartVspTinv_array       = [] #nPart Vs 1/pT
    nPartVspTinvErr_array    = []


    ###arguments###
    ap = argparse.ArgumentParser()
    ap.add_argument("-iF",  "--iFile", required=True, help="Specify the input File to Read (e.g: hits.txt)")    
    ap.add_argument("-iL",  "--iLayer", required=False, help="Specify the Layer Number")    
    ap.add_argument("-iPR",  "--iPartRange", required=False, help="Specify the range of particles required (default = [-1, 99999] means all")    
    ap.add_argument("-pTB",  "--pTBins", required=False, help="Specify pT bins (default ['0.0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0','1.1'])") 
    ap.add_argument("-pTinvB",  "--pTinvBins", required=False, help="Specify pTinv bins (default ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10','100'])")

    ap.add_argument("-v", "--verbose", required=False, help="verbose, (default = False)")
    ap.add_argument("-od", "--outputdir", required=False, help="output directory (default=Plotter/ outputdir Must exist")
    ap.add_argument("-pS", "--plotShapes", required=False, help="plot shapes of each cluster...may be extremely slow depending on the number of shapes (default False)")
    ap.add_argument("-pTSel",  "--pTSel", required=False, help="Specify [pTMin, pTMax] (default 0, 1000 Gev)")
    ap.add_argument("-subdir",  "--subdir", required=False, help="Specify subdir where to store shapes (default Shapes/)")

    args = vars(ap.parse_args())

    if args["iFile"]:
      ifile = args["iFile"]
    if args["iFile"]:
      ifile = args["iFile"]
    if args["iLayer"]:
      layerN = int(args["iLayer"])
    if args["iPartRange"]:
      iPartRange[0] = int((args["iPartRange"]).split(",")[0])
      iPartRange[1] = int((args["iPartRange"]).split(",")[1])
      if iPartRange[0] >= 0:
          nPart = iPartRange[1] - iPartRange[0] + 1
    if args["pTBins"]:
        pT_bins = (args["pTBins"]).split(",")
    else:
        pT_bins = ['0.0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '1.0','1.1'] #[0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    if args["pTinvBins"]:
        pTinv_bins = (args["pTinvBins"]).split(",")
    else:
        pTinv_bins = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10','100']
    if args["verbose"]:
      verbose = strtobool(args["verbose"])
    if args["outputdir"]:
      dir = args["outputdir"]
    if args["plotShapes"]:
      plotShapes = strtobool(args["plotShapes"])
    if args["pTSel"]:
      pTSel[0] = float(args["pTSel"].split(",")[0])
      pTSel[1] = float(args["pTSel"].split(",")[1])
    if args["subdir"]:
      subdir = args["subdir"]


    if verbose:
        print ("pT_bins",pT_bins)

    ###0) remove particle outside acceptance (no hits)
    ifile_new = removePartNoHits(ifile)
    ###1) read particle Info and hits from txt file
    hits, partInfo = readHits(ifile_new, iPartRange, pTSel, False)
    if verbose:
        print ("partInfo=",partInfo)
        print ("hits=",hits)
    if len(hits) != len(partInfo):
        print ("hits and partInfo cannot have different lenghts",len(hits),len(partInfo))

    if verbose:
        print ("nPart=",nPart)
    if iPartRange[0] < -1 and iPartRange[1] >= 99999:
        npart = len(hits)
    elif nPart <=len(hits) and nPart >=0:
        npart = nPart
    else:
        npart = len(hits)

    if verbose:
        print ("npart=",npart)



    for ip in range(0,npart):
        pX_array.append(float(partInfo[ip][3]))
        pY_array.append(float(partInfo[ip][4]))
        pZ_array.append(float(partInfo[ip][5]))

    if verbose:
        print ("***hits***")
        for ip in range(0,npart):
            print ("---particle---",ip)
            for ih in range(0,len(hits[ip])):
                print ("hits",hits[ip][ih])

    ###can select layer here (layerN<0 will included all layers)
    ### 2) #hits per pT bin. Multiple entries if more particle within pT bin
    nHitsVspT_array, nHitsVspTinv_array = nHitsVspT(pT_bins, pTinv_bins, hits, layerN, partInfo, verbose)
    
    ### 3) compute mean, StdDev within pT bin of nHits and nPart
    nHitsMean_array, nHitsStddev_array = compMeanStdDev(nHitsVspT_array, verbose)
    nHitsVspTinvMean_array, nHitsVspTinvStddev_array = compMeanStdDev(nHitsVspTinv_array, verbose)
    for iptb in range(0, len(nHitsVspT_array)):
        #print ("ptb",nHitsVspT_array[iptb])
        nPartVspT_array.append(nHitsVspT_array[iptb][1])
        nPartVspTErr_array.append(math.sqrt(nHitsVspT_array[iptb][1]))
    for iptinvb in range(0, len(nHitsVspTinv_array)):
        nPartVspTinv_array.append(nHitsVspTinv_array[iptinvb][1])
        nPartVspTinvErr_array.append(math.sqrt(nHitsVspTinv_array[iptinvb][1]))

    ### 4) plot <nHits> Vs pT
    plot2D(pT_bins,nHitsMean_array,nHitsStddev_array,"pT [GeV]",dir+"nHitsLayerVspT"+str(layerN)+".jpg")
    plot2D(pTinv_bins,nHitsVspTinvMean_array,nHitsVspTinvStddev_array,"1/pT [1/GeV]",dir+"nHitsLayerVspTinv"+str(layerN)+".jpg")
    plot2D(pT_bins,nPartVspT_array,nPartVspTErr_array,"pT [GeV]",dir+"nPartLayerVspT"+str(layerN)+".jpg",verbose)
    plot2D(pTinv_bins,nPartVspTinv_array,nPartVspTinvErr_array,"1/pT [1/GeV]",dir+"nPartLayerVspTinv"+str(layerN)+".jpg",verbose)

    ### 5) store hits in RowColSegHitLayer
    for ip in range(0,npart):
        RowColSegHitLayer.append(HitsPerLayer(hits[ip],layerN))

    ### 6) plot hit shapes in local coordinates
    if plotShapes:
        for ip in range(0,npart):
            if verbose:
                print ("plot hit shapes for particle",ip)
            pnum = ip
            if iPartRange[0] > 0:
                pnum = ip+iPartRange[0]

            str_tmp = "p"+str(pnum)+"-PX="+str(pX_array[ip])+"_PY="+str(pY_array[ip])+"_PZ="+str(pZ_array[ip])+"-L="+str(layerN)
            rowBoundaries_tmp, colBoundaries_tmp, seg_tmp, matrix_tmp = generateSensorMatrix(RowColSegHitLayer[ip], nRow, nCol, verbose)
            if verbose:
                print ("rowBoundaries_tmp",rowBoundaries_tmp)
                print ("colBoundaries_tmp",colBoundaries_tmp)
            matrix.append([rowBoundaries_tmp, colBoundaries_tmp, seg_tmp, matrix_tmp])

            plotImage(matrix[ip],"jet",str_tmp,dir+subdir+str_tmp,True,False)

            if verbose:
                print ("ip",ip,"matrix row, col boundaries, seg=",matrix[ip][0],matrix[ip][1], matrix[ip][2])


