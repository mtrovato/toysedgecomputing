#! /bin/bash

#./PrepareInputCNN.sh "signal"; ./PrepareInputCNN.sh "background";
#specify: 1) outputDir, 2) hitFiles, 3) particle ranges before running

#not relevant
pTB="0.00, 0.09, 0.1,0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1"
pTinvB="0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100"


#if defining signal and background from the same hitfile specify pT selection and have hitFileSg=hitFileBg
pTSelSg=""
pTSelBg=""

outDir="Plotter2000particles/"
hitFileDir="hitfiles/"

subDirTrain="train/"
subDirTest="test/"
#check number of particles first by doing grep "Part" $hitFileDir/$hitFile | grep mu |wc
nPartBg=1900 #bg =975
iPartRangeTrainBg="0, 999"  #default [-1, 9999] select particle range (extreme included)
iPartRangeTestBg="1001, $((nPartBg-1))"  #default [-1, 9999] select particle range (extreme included)
nPartSg=2000
iPartRangeTrainSg="0, 999"  
iPartRangeTestSg="1000, $((nPartSg-1))" 

#background
#hitFile="hits1000particles_GenRandY0_02to1RandZ0to0_5.txt"
hitFileSg="hits2000particles_GenRandY0_4to1_0RandZ0to0_5.txt"
subDirSg="signal/"
#signal
hitFileBg="hits2000particles_GenRandY0_1to0_2RandZ0to0_5.txt"
subDirBg="background/"

if [ $# != 1 ]
then
    echo "you should pass one and only one argument"
    exit 2
fi

if [ $1 == "signal" ] 
then
    hitFile=$hitFileSg
    subDir=$subDirSg
    pTSel=$pTSelSg
    iPartRangeTrain=$iPartRangeTrainSg
    iPartRangeTest=$iPartRangeTestSg
elif [ $1 == "background" ] 
then
    hitFile=$hitFileBg
    subDir=$subDirBg
    pTSel=$pTSelBg
    iPartRangeTrain=$iPartRangeTrainBg
    iPartRangeTest=$iPartRangeTestBg
else
    echo "$1 not allowed"
    exit 1
fi


# if [ $# != 1 ]
# then
#     echo "you should pass one and only one argument"
#     exit 2
# fi

# if [ $1 == "lowpt" ] 
# then
#     subDir="lowpt/"
# #    pTSel="0.0, 0.3" 
# elif [ $1 == "highpt" ] 
# then
#     subDir="highpt/"
# #    pTSel="0.35, 1000" 
# else
#     echo "$1 not allowed"
#     exit 1
# fi


if [ ! -d $outDir ]; then
    mkdir $outDir
fi
if [ ! -d $outDir/$subDirTrain ]; then
    mkdir $outDir/$subDirTrain
fi
if [ ! -d $outDir/$subDirTest ]; then
    mkdir $outDir/$subDirTest
fi
if [ ! -d $outDir/$subDirTrain/$subDir ]; then
    mkdir $outDir/$subDirTrain/$subDir
fi
if [ ! -d $outDir/$subDirTest/$subDir ]; then
    mkdir $outDir/$subDirTest/$subDir
fi
cp $hitFileDir/$hitFile $outDir

pathOutDir_a=($outDir/$subDirTrain $outDir/$subDirTest)
pathSubDir_a=($outDir/$subDirTrain/$subDir $outDir/$subDirTest/$subDir)
iPartRangeTrain_a=("$iPartRangeTrain" "$iPartRangeTest")
for (( is=0; is<2; is++ )) #Train Test
do
    for (( i=0; i<1; i++ )) #Layer 0 only
    do
	pathHitFile=$outDir/$hitFile
    
	pathOutDir=${pathOutDir_a[is]} #$outDir/$subDirTrain
	pathSubDir=${pathSubDir_a[is]}
	iPartRange=${iPartRangeTrain_a[is]}
	cmd="python3 Plotter.py -iF $pathHitFile -iL $i -od $pathOutDir -pS True -iPR \"${iPartRange}\" -subdir $subDir"
	echo $cmd
	eval "$cmd"
	extraDir=$outDir/"extra_plots_train"_$subDir
	if [ ! -d $extraDir ]; then
	    mkdir $extraDir
	fi
	mv $pathOutDir/*.jpg $extraDir

    done
done

cp PrepareInputCNN.sh $outDir
#rm $outDir/$subDirTrain/*.jpg
#rm $outDir/$subDirTest/*.jpg
