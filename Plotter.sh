#! /bin/bash

plotShapes=True #slow

if [ $# != 1 ]
then
    echo "you should pass one and only one argument"
    exit 2
fi

if [ $1 == "60p" ] 
then
    echo "0.1 GeV to 1 GeV"
    str_tmp=60particles_PY01_to1GeV_PZ0to05GeV
    pTB="0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1"
    pTinvB="0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10"
elif [ $1 == "48p" ] 
then
    echo "0.02 to 0.09 Gev  (0.01 GeV does not reach layer 0)"
    str_tmp="48particles_PY0_02_to0_09GeV_PZ0to05GeV"
    pTB="0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1"
    pTinvB="0, 10, 2, 3, 4, 5, 6, 7, 8, 9, 100"
elif [ $1 == "1000p" ] 
then
    echo "PX=0 GeV, PY=0.02 to 1 Gev, PZ = 0.0 to 0.5 GeV"
    str_tmp="1000particles_GenRandY0_02to1RandZ0to0_5"
    pTB="0.00, 0.09, 0.1,0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1"
    pTinvB="0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100"
else
    echo "$1 not allowed"
fi


dir="Plotter${str_tmp}" #already exists 
mkdir $dir/"Shapes/"
hitfile="hits${str_tmp}.txt" #it already exists in dir

for (( i=-1; i<5; i++ ))
do
    echo "python3 Plotter.py -iF $dir/$hitfile -iL $i -pTB "$pTB" -pTinvB "$pTinvB" -od "$dir/" -pS ${plotShapes}"
    python3 Plotter.py -iF $dir/$hitfile -iL $i -pTB "$pTB" -pTinvB "$pTinvB" -od "$dir/" -pS ${plotShapes}
done


#check particle w/o and w/ error
#grep "0.0 ,  0.0 ,  0.0" hits_GenRandY0_02to1RandZ0to0_5_1000_tmp.csv | grep ",  0$" |wc
#hits_GenRandY0_02to1RandZ0to0_5_1000_tmp.csv | grep ",  -1$" |wc #-2, -3, -4, -5, -6, -7
#grep "0.0 ,  0.0 ,  0.0" hits_GenRandY0_02to1RandZ0to0_5_1000_tmp.csv | grep ",  1$" | grep ",  -1$"| wc

